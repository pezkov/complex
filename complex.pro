TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += C++11
QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp \
    complex.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    complex.h

