#ifndef COMPLEX_H
#define COMPLEX_H
#include <cmath>
template <typename T>
class complex
{
    T _re;
    T _im;
public:
    complex() = default;
    complex(T re, T im):_re(re), _im(im){}

    const T& real(){
        return _re;
    }
    const T& imag(){
        return _im;
    }

    void real(T re){
        _re = re;
    }

    void imag(T im){
        _im = im;
    }

    friend complex<T> operator*(complex<T> c1, complex<T> c2){
        return complex<T>(c1.real()*c2.real() - c1.imag() * c2.imag(), c1.real()*c2.imag() + c1.imag()*c2.real());
    }
    friend complex<T> operator*(complex<T> c1, float n){
        return complex<T>(c1.real()*n, c1.imag()*n);
    }
    friend complex<float> exp(complex<T> val){
        return complex<float>(cos(val.imag())*std::exp(val.real()), sin(val.imag())*std::exp(val.real()));
    }
    friend complex<T> conj(complex<T> c){
        return complex<T>(c.real(),-c.imag());
    }

};



#endif // COMPLEX_H
